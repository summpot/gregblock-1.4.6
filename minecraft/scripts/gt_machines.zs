import mods.gregtech.recipe.RecipeMap;

val hammer = RecipeMap.getByName("forge_hammer");
val compressor = RecipeMap.getByName("compressor");
val macerator = RecipeMap.getByName("macerator");
val assembler = RecipeMap.getByName("assembler");
val alloy = RecipeMap.getByName("alloy_smelter");
val extractor = RecipeMap.getByName("extractor");
val lathe = RecipeMap.getByName("lathe");
val canner = RecipeMap.getByName("canner");
val fluid_canner = RecipeMap.getByName("fluid_canner");
val fluid_extractor = RecipeMap.getByName("fluid_extractor");
val freezer = RecipeMap.getByName("vacuum_freezer");
val mixer = RecipeMap.getByName("mixer");
val thermal_sep = RecipeMap.getByName("thermal_centrifuge");
val sifter = RecipeMap.getByName("sifter");
val autoclave = RecipeMap.getByName("autoclave");
val reactor = RecipeMap.getByName("chemical_reactor");
val solidifier = RecipeMap.getByName("fluid_solidifier");
val saw = RecipeMap.getByName("cutting_saw");
val forming = RecipeMap.getByName("forming_press");
val electrolyzer = RecipeMap.getByName("electrolyzer");
val circuit_assembler = RecipeMap.getByName("circuit_assembler");
val implosion = RecipeMap.getByName("implosion_compressor");
val engraver = RecipeMap.getByName("laser_engraver");
val attractor = RecipeMap.getByName("attractor");
val packer = RecipeMap.getByName("packer");
val arc = RecipeMap.getByName("arc_furnace");
val plasma_arc = RecipeMap.getByName("plasma_arc_furnace");

//Ex Nihilo
hammer.recipeBuilder().inputs([<ore:gravel>]).outputs([<minecraft:sand>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:sand>]).outputs([<exnihilocreatio:block_dust>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:netherrack>]).outputs([<exnihilocreatio:block_netherrack_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:endstone>]).outputs([<exnihilocreatio:block_endstone_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<appliedenergistics2:sky_stone_block>]).outputs([<exnihilocreatio:block_skystone_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:stoneDiorite>]).outputs([<exnihilocreatio:block_diorite_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:stoneAndesite>]).outputs([<exnihilocreatio:block_andesite_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<ore:stoneGranite>]).outputs([<exnihilocreatio:block_granite_crushed>]).duration(16).EUt(10).buildAndRegister();
hammer.recipeBuilder().inputs([<exnihilocreatio:block_granite_crushed>]).outputs([<minecraft:sand:1>]).duration(16).EUt(10).buildAndRegister();

//IF
hammer.recipeBuilder().inputs([<ore:platePlastic>*2]).outputs([<industrialforegoing:plastic>]).duration(300).EUt(16).buildAndRegister();
assembler.recipeBuilder().inputs([<ore:platePlastic>*8]).outputs([<teslacorelib:machine_case>]).property("circuit",8).duration(50).EUt(16).buildAndRegister();
solidifier.recipeBuilder().fluidInputs([<liquid:if.pink_slime>*144]).notConsumable(<gregtech:meta_item_1:32306>).outputs([<industrialforegoing:pink_slime_ingot>]).duration(20).EUt(8).buildAndRegister();

//Useful
lathe.recipeBuilder().inputs([<minecraft:stone>]).outputs([<exnihilocreatio:item_material:6>,<gregtech:meta_item_1:1328>*2]).duration(500).EUt(16).buildAndRegister();

//Fix
macerator.recipeBuilder().inputs([<appliedenergistics2:material:7>]).outputs([<appliedenergistics2:material:8>]).duration(30).EUt(8).buildAndRegister();

//Porcelain Brick
alloy.recipeBuilder().inputs([<ore:clayPorcelain>]).notConsumable(<gregtech:meta_item_1:32306>).outputs([<ceramics:unfired_clay:5>]).duration(200).EUt(2).buildAndRegister();

//Glasses
recipes.remove(<appliedenergistics2:quartz_glass>);
alloy.recipeBuilder().inputs([<ore:dustNetherQuartz>*4,<minecraft:glass>*5]).outputs([<appliedenergistics2:quartz_glass>*5]).duration(250).EUt(16).buildAndRegister();
recipes.remove(<appliedenergistics2:quartz_vibrant_glass>);
alloy.recipeBuilder().inputs([<ore:dustGlowstone>*2,<appliedenergistics2:quartz_glass>]).outputs([<appliedenergistics2:quartz_vibrant_glass>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustCopper>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustTin>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:1>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustSilver>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:2>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustLead>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:3>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustAluminium>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:4>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustNickel>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:5>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustPlatinum>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:6>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustIridium>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass:7>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustSteel>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustElectrum>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:1>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustInvar>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:2>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustBronze>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:3>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustSignalum>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:5>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustLumium>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:6>]).duration(50).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustEnderium>*4,<minecraft:glass>]).outputs([<thermalfoundation:glass_alloy:7>]).duration(50).EUt(16).buildAndRegister();

//NuclearCraft Stuff
recipes.remove(<nuclearcraft:reactor_casing_transparent>);
alloy.recipeBuilder().inputs([<nuclearcraft:fission_block>,<minecraft:glass>]).outputs([<nuclearcraft:reactor_casing_transparent>]).duration(50).EUt(16).buildAndRegister();
recipes.remove(<nuclearcraft:part>);
alloy.recipeBuilder().inputs([<ore:plateLead>,<ore:dustGraphite>]).outputs([<nuclearcraft:part>]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:ingotFerroboron>,<ore:ingotLithium>]).outputs([<nuclearcraft:alloy:1>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustFerroboron>,<ore:ingotLithium>]).outputs([<nuclearcraft:alloy:1>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:ingotFerroboron>,<ore:dustLithium>]).outputs([<nuclearcraft:alloy:1>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustFerroboron>,<ore:dustLithium>]).outputs([<nuclearcraft:alloy:1>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:ingotSteel>,<ore:ingotBoron>]).outputs([<nuclearcraft:alloy:6>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustSteel>,<ore:ingotBoron>]).outputs([<nuclearcraft:alloy:6>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:ingotSteel>,<ore:dustBoron>]).outputs([<nuclearcraft:alloy:6>*2]).duration(300).EUt(16).buildAndRegister();
alloy.recipeBuilder().inputs([<ore:dustSteel>,<ore:dustBoron>]).outputs([<nuclearcraft:alloy:6>*2]).duration(300).EUt(16).buildAndRegister();

canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustRedstone>*16]).outputs([<nuclearcraft:cooler:2>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustNetherQuartz>*16]).outputs([<nuclearcraft:cooler:3>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustGold>*16]).outputs([<nuclearcraft:cooler:4>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustGlowstone>*16]).outputs([<nuclearcraft:cooler:5>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustLapis>*16]).outputs([<nuclearcraft:cooler:6>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustDiamond>*16]).outputs([<nuclearcraft:cooler:7>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustEnderium>*16]).outputs([<nuclearcraft:cooler:9>]).duration(400).EUt(9).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustIron>*16]).outputs([<nuclearcraft:cooler:11>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustEmerald>*16]).outputs([<nuclearcraft:cooler:12>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustCopper>*16]).outputs([<nuclearcraft:cooler:13>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustTin>*16]).outputs([<nuclearcraft:cooler:14>]).duration(400).EUt(2).buildAndRegister();
canner.recipeBuilder().inputs([<nuclearcraft:cooler>,<ore:dustMagnesium>*16]).outputs([<nuclearcraft:cooler:15>]).duration(400).EUt(2).buildAndRegister();
fluid_canner.recipeBuilder().inputs([<nuclearcraft:cooler>]).fluidInputs([<liquid:water>*1000]).outputs([<nuclearcraft:cooler:1>]).duration(400).EUt(2).buildAndRegister();
fluid_canner.recipeBuilder().inputs([<nuclearcraft:cooler>]).fluidInputs([<liquid:liquidhelium>*1000]).outputs([<nuclearcraft:cooler:8>]).duration(400).EUt(2).buildAndRegister();
fluid_canner.recipeBuilder().inputs([<nuclearcraft:cooler>]).fluidInputs([<liquid:cryotheum>*1000]).outputs([<nuclearcraft:cooler:10>]).duration(400).EUt(2).buildAndRegister();

extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:2>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:3>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:4>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:5>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:6>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:7>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:9>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:11>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:12>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:13>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:14>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:15>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:8>]).duration(400).EUt(2).buildAndRegister();
extractor.recipeBuilder().outputs([<nuclearcraft:cooler>]).inputs([<nuclearcraft:cooler:10>]).duration(400).EUt(2).buildAndRegister();

fluid_extractor.recipeBuilder().inputs([<ore:dustCryotheum>]).fluidOutputs([<liquid:cryotheum>*250]).duration(40).EUt(32).buildAndRegister();
fluid_extractor.recipeBuilder().inputs([<ore:dustPyrotheum>]).fluidOutputs([<liquid:pyrotheum>*250]).duration(40).EUt(32).buildAndRegister();
fluid_extractor.recipeBuilder().inputs([<ore:dustAerotheum>]).fluidOutputs([<liquid:aerotheum>*250]).duration(40).EUt(32).buildAndRegister();
fluid_extractor.recipeBuilder().inputs([<ore:dustPetrotheum>]).fluidOutputs([<liquid:petrotheum>*250]).duration(40).EUt(32).buildAndRegister();
fluid_extractor.recipeBuilder().inputs([<ore:gemEnderPearl>]).fluidOutputs([<liquid:ender>*250]).duration(40).EUt(32).buildAndRegister();
freezer.recipeBuilder().fluidInputs([<liquid:helium>*4000]).fluidOutputs([<liquid:liquidhelium>*4000]).duration(400).EUt(120).buildAndRegister();

//Fuels
compressor.recipeBuilder().inputs(<ore:dustUranium>).outputs(<nuclearcraft:uranium:8>).duration(300).EUt(2).buildAndRegister();
compressor.recipeBuilder().inputs(<ore:dustPlutonium>).outputs(<nuclearcraft:plutonium>).duration(300).EUt(2).buildAndRegister();
compressor.recipeBuilder().inputs(<ore:dustThorium>*6).outputs(<nuclearcraft:fuel_thorium>).duration(1800).EUt(2).buildAndRegister();
compressor.recipeBuilder().inputs(<ore:dustNaquadahEnriched>*6).outputs(<nuclearcraft:fuel_uranium:4>).duration(1800).EUt(2).buildAndRegister();

thermal_sep.recipeBuilder().inputs(<nuclearcraft:depleted_fuel_thorium>).outputs(<gregtech:meta_item_1:2037>,<nuclearcraft:thorium>*2).duration(500).EUt(48).buildAndRegister();
thermal_sep.recipeBuilder().inputs(<nuclearcraft:depleted_fuel_uranium:2>).outputs(<nuclearcraft:plutonium:2>,<nuclearcraft:uranium:8>*4).duration(500).EUt(48).buildAndRegister();
thermal_sep.recipeBuilder().inputs(<nuclearcraft:depleted_fuel_mixed_oxide>).outputs(<nuclearcraft:plutonium:10>,<nuclearcraft:plutonium>*3).duration(500).EUt(48).buildAndRegister();
thermal_sep.recipeBuilder().inputs(<nuclearcraft:depleted_fuel_uranium:4>).outputs(<gregtech:meta_item_1:2307>*2,<gregtech:meta_item_1:2310>).duration(500).EUt(48).buildAndRegister();

//Thermal Alloys
recipes.remove(<ore:dustEnderium>);
mixer.recipeBuilder().inputs([<ore:dustLead>*3,<ore:dustPlatinum>]).fluidInputs([<liquid:ender>*1000]).outputs([<thermalfoundation:material:103>*4]).duration(400).EUt(8).buildAndRegister();
mixer.recipeBuilder().inputs([<ore:dustSmallLead>*3,<ore:dustSmallPlatinum>]).fluidInputs([<liquid:ender>*250]).outputs([<thermalfoundation:material:103>]).duration(100).EUt(8).buildAndRegister();
recipes.remove(<ore:dustSignalum>);
mixer.recipeBuilder().inputs([<ore:dustCopper>*3,<ore:dustSilver>]).fluidInputs([<liquid:redstone>*1000]).outputs([<thermalfoundation:material:101>*4]).duration(400).EUt(8).buildAndRegister();
mixer.recipeBuilder().inputs([<ore:dustSmallCopper>*3,<ore:dustSmallSilver>]).fluidInputs([<liquid:redstone>*250]).outputs([<thermalfoundation:material:101>]).duration(100).EUt(8).buildAndRegister();
recipes.remove(<ore:dustLumium>);
mixer.recipeBuilder().inputs([<ore:dustTin>*3,<ore:dustSilver>]).fluidInputs([<liquid:glowstone>*1000]).outputs([<thermalfoundation:material:102>*4]).duration(400).EUt(8).buildAndRegister();
mixer.recipeBuilder().inputs([<ore:dustSmallTin>*3,<ore:dustSmallSilver>]).fluidInputs([<liquid:glowstone>*250]).outputs([<thermalfoundation:material:102>]).duration(100).EUt(8).buildAndRegister();

//Thermal Blocks
macerator.recipeBuilder().inputs([<thermalfoundation:storage_alloy:5>]).outputs([<thermalfoundation:material:101>*9]).duration(270).EUt(32).buildAndRegister();
macerator.recipeBuilder().inputs([<thermalfoundation:storage_alloy:6>]).outputs([<thermalfoundation:material:102>*9]).duration(270).EUt(32).buildAndRegister();
macerator.recipeBuilder().inputs([<thermalfoundation:storage_alloy:7>]).outputs([<thermalfoundation:material:103>*9]).duration(270).EUt(32).buildAndRegister();

//AE2 Compatability
recipes.remove(<appliedenergistics2:material>);
furnace.remove(<gregtech:meta_item_1:8202>);
furnace.addRecipe(<appliedenergistics2:material>,<ore:oreCertusQuartz>);
furnace.addRecipe(<appliedenergistics2:material>,<gregblockutils:gb_meta_item:202>);
furnace.addRecipe(<appliedenergistics2:material>,<gregblockutils:gb_meta_item:1202>);
furnace.addRecipe(<appliedenergistics2:material>,<gregblockutils:gb_meta_item:2202>);
furnace.addRecipe(<appliedenergistics2:material>,<gregblockutils:gb_meta_item:3202>);
sifter.findRecipe(12800,[<gregtech:meta_item_1:6202>],[null]).remove();
sifter.recipeBuilder().inputs([<ore:crushedPurifiedCertusQuartz>]).chancedOutput(<gregtech:meta_item_2:23202>,2000).chancedOutput(<gregtech:meta_item_2:25202>,100).chancedOutput(<gregtech:meta_item_1:4202>,5000).chancedOutput(<appliedenergistics2:material>,1500).chancedOutput(<gregtech:meta_item_2:22202>,4000).chancedOutput(<gregtech:meta_item_2:24202>,400).EUt(16).duration(800).buildAndRegister();
autoclave.findRecipe(24,[<appliedenergistics2:material:2>],[<liquid:water>*200]).remove();
autoclave.findRecipe(24,[<appliedenergistics2:material:2>],[<liquid:distilled_water>*200]).remove();
autoclave.recipeBuilder().inputs([<ore:dustCertusQuartz>]).fluidInputs([<liquid:water>*200]).outputs([<appliedenergistics2:material>]).EUt(24).duration(1500).buildAndRegister();
autoclave.recipeBuilder().inputs([<ore:dustCertusQuartz>]).fluidInputs([<liquid:distilled_water>*200]).outputs([<appliedenergistics2:material>]).EUt(24).duration(1500).buildAndRegister();
reactor.findRecipe(30,[<appliedenergistics2:material:2>*3,<gregtech:meta_item_1:2063>],[<liquid:water>*1000]).remove();
reactor.findRecipe(30,[<appliedenergistics2:material:2>*3,<gregtech:meta_item_1:2063>],[<liquid:distilled_water>*1000]).remove();
reactor.recipeBuilder().inputs([<appliedenergistics2:material:2>*3,<gregtech:meta_item_1:2063>]).fluidInputs([<liquid:water>*1000]).outputs([<appliedenergistics2:material>]).EUt(30).duration(500).buildAndRegister();
reactor.recipeBuilder().inputs([<appliedenergistics2:material:2>*3,<gregtech:meta_item_1:2063>]).fluidInputs([<liquid:distilled_water>*1000]).outputs([<appliedenergistics2:material>]).EUt(30).duration(500).buildAndRegister();
hammer.recipeBuilder().inputs([<gregtech:compressed_9:3>]).outputs(<appliedenergistics2:material>*9).EUt(24).duration(100).buildAndRegister();
compressor.recipeBuilder().inputs([<appliedenergistics2:material>*9]).outputs([<gregtech:compressed_9:3>]).EUt(2).duration(400).buildAndRegister();
recipes.remove(<appliedenergistics2:fluix_block>);
recipes.remove(<minecraft:quartz_block>);
compressor.recipeBuilder().inputs([<appliedenergistics2:material:7>*4]).outputs([<appliedenergistics2:fluix_block>]).EUt(2).duration(400).buildAndRegister();
hammer.recipeBuilder().inputs([<appliedenergistics2:fluix_block>]).outputs([<appliedenergistics2:material:7>*4]).EUt(24).duration(100).buildAndRegister();

//Storage Drawers
saw.recipeBuilder().inputs([<ore:drawerTrim>]).outputs([<storagedrawers:upgrade_template>*2]).EUt(4).duration(50).buildAndRegister();

//AE2 Recipes
assembler.recipeBuilder().inputs([<appliedenergistics2:part:140>*2,<ore:dustFluix>]).outputs(<appliedenergistics2:part:16>).EUt(8).duration(30).buildAndRegister();
recipes.remove(<appliedenergistics2:part:140>);
autoclave.recipeBuilder().inputs([<ore:boltNetherQuartz>*4]).fluidInputs([<liquid:glass>*72]).outputs(<appliedenergistics2:part:140>).EUt(8).duration(30).buildAndRegister();

forming.recipeBuilder().inputs([<ore:plateCertusQuartz>]).notConsumable(<appliedenergistics2:material:13>).outputs([<appliedenergistics2:material:16>]).EUt(32).duration(100).buildAndRegister();
forming.recipeBuilder().inputs([<ore:plateDiamond>]).notConsumable(<appliedenergistics2:material:14>).outputs([<appliedenergistics2:material:17>]).EUt(32).duration(100).buildAndRegister();
forming.recipeBuilder().inputs([<ore:plateGold>]).notConsumable(<appliedenergistics2:material:15>).outputs([<appliedenergistics2:material:18>]).EUt(32).duration(100).buildAndRegister();
forming.recipeBuilder().inputs([<ore:plateSilicon>]).notConsumable(<appliedenergistics2:material:19>).outputs([<appliedenergistics2:material:20>]).EUt(32).duration(100).buildAndRegister();

circuit_assembler.recipeBuilder().inputs([<appliedenergistics2:material:16>,<appliedenergistics2:material:20>]).fluidInputs([<liquid:redstone>*144]).outputs([<appliedenergistics2:material:23>]).EUt(96).duration(300).buildAndRegister();
circuit_assembler.recipeBuilder().inputs([<appliedenergistics2:material:17>,<appliedenergistics2:material:20>]).fluidInputs([<liquid:redstone>*144]).outputs([<appliedenergistics2:material:24>]).EUt(96).duration(300).buildAndRegister();
circuit_assembler.recipeBuilder().inputs([<appliedenergistics2:material:18>,<appliedenergistics2:material:20>]).fluidInputs([<liquid:redstone>*144]).outputs([<appliedenergistics2:material:22>]).EUt(96).duration(300).buildAndRegister();

mixer.recipeBuilder().inputs([<appliedenergistics2:material:1>,<ore:dustRedstone>,<ore:gemNetherQuartz>]).fluidInputs([<liquid:water>*0]).outputs([<appliedenergistics2:material:7>*2]).EUt(8).duration(200).buildAndRegister();

electrolyzer.recipeBuilder().inputs([<appliedenergistics2:material>]).outputs([<appliedenergistics2:material:1>]).EUt(116).duration(60).buildAndRegister();

implosion.recipeBuilder().inputs([<minecraft:iron_ingot>]).outputs(<gregtech:meta_item_1:10700>).EUt(30).duration(20).buildAndRegister();
implosion.recipeBuilder().inputs([<ore:blockIron>]).outputs(<pneumaticcraft:compressed_iron_block>).EUt(30).duration(20).buildAndRegister();

forming.findRecipe(16,[<gregtech:meta_item_1:12094>,<gregtech:meta_item_1:32304>],[null]).remove();

//Forestry Automation
fluid_extractor.recipeBuilder().inputs([<forestry:crafting_material:5>]).fluidOutputs([<liquid:ice>]).EUt(128).duration(128).buildAndRegister();
assembler.recipeBuilder().inputs([<ore:plateBrass>*8]).outputs([<forestry:sturdy_machine>]).property("circuit",8).duration(50).EUt(16).buildAndRegister();
assembler.recipeBuilder().inputs([<forestry:sturdy_machine>,<ore:plateTin>*4,<ore:plateWroughtIron>*4]).outputs([<genetics:misc>]).duration(50).EUt(16).buildAndRegister();
assembler.recipeBuilder().inputs([<genetics:misc>,<ore:plateDiamond>*8]).fluidInputs([<liquid:water>*5000]).outputs([<forestry:hardened_machine>]).duration(50).EUt(64).buildAndRegister();

//AE2 Skyblockification
mods.appliedenergistics2.Inscriber.removeRecipe(<appliedenergistics2:material:13>);
mods.appliedenergistics2.Inscriber.removeRecipe(<appliedenergistics2:material:14>);
mods.appliedenergistics2.Inscriber.removeRecipe(<appliedenergistics2:material:15>);
mods.appliedenergistics2.Inscriber.removeRecipe(<appliedenergistics2:material:19>);

engraver.recipeBuilder().inputs([<ore:plateDenseTantalum>]).notConsumable(<ore:craftingLensRed>).outputs([<appliedenergistics2:material:14>]).EUt(120).duration(3600).buildAndRegister();
engraver.recipeBuilder().inputs([<ore:plateDenseTantalum>]).notConsumable(<ore:craftingLensBlue>).outputs([<appliedenergistics2:material:13>]).EUt(120).duration(3600).buildAndRegister();
engraver.recipeBuilder().inputs([<ore:plateDenseTantalum>]).notConsumable(<ore:craftingLensWhite>).outputs([<appliedenergistics2:material:19>]).EUt(120).duration(3600).buildAndRegister();
engraver.recipeBuilder().inputs([<ore:plateDenseTantalum>]).notConsumable(<ore:craftingLensLime>).outputs([<appliedenergistics2:material:15>]).EUt(120).duration(3600).buildAndRegister();

//Automated Casing Recipes
assembler.recipeBuilder().inputs([<ore:plateBasic>*4,<ore:plateTough>*4]).outputs([<nuclearcraft:fission_block>*4]).property("circuit",8).duration(50).EUt(16).buildAndRegister();

assembler.recipeBuilder().inputs([<ore:plateGlass>*4,<ore:plateTough>*4]).outputs([<nuclearcraft:cell_block>]).property("circuit",8).duration(50).EUt(16).buildAndRegister();

assembler.recipeBuilder().inputs([<ore:plateIronCompressed>*8]).outputs([<pneumaticcraft:pressure_chamber_wall>*4]).property("circuit",8).duration(50).EUt(16).buildAndRegister();

//Ducts
recipes.remove(<thermaldynamics:duct_0:2>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_0:6>]).fluidInputs([<liquid:redstone>*200]).outputs([<thermaldynamics:duct_0:2>]).duration(80).EUt(2).buildAndRegister();
recipes.remove(<thermaldynamics:duct_0:3>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_0:7>]).fluidInputs([<liquid:redstone>*200]).outputs([<thermaldynamics:duct_0:3>]).duration(80).EUt(2).buildAndRegister();
recipes.remove(<thermaldynamics:duct_0:4>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_0:8>]).fluidInputs([<liquid:redstone>*200]).outputs([<thermaldynamics:duct_0:4>]).duration(80).EUt(2).buildAndRegister();
recipes.remove(<thermaldynamics:duct_0:5>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_0:9>]).fluidInputs([<liquid:cryotheum>*500]).outputs([<thermaldynamics:duct_0:5>]).duration(200).EUt(2).buildAndRegister();
recipes.removeByRecipeName("thermaldynamics:duct_32_10");
recipes.removeByRecipeName("thermaldynamics:duct_32_16");
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_32>]).fluidInputs([<liquid:glowstone>*200]).outputs([<thermaldynamics:duct_32:2>]).duration(80).EUt(2).buildAndRegister();
recipes.removeByRecipeName("thermaldynamics:duct_32_11");
recipes.removeByRecipeName("thermaldynamics:duct_32_17");
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_32:1>]).fluidInputs([<liquid:glowstone>*200]).outputs([<thermaldynamics:duct_32:3>]).duration(80).EUt(2).buildAndRegister();
recipes.removeByRecipeName("thermaldynamics:duct_32_12");
recipes.removeByRecipeName("thermaldynamics:duct_32_20");
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_32:4>]).fluidInputs([<liquid:glowstone>*200]).outputs([<thermaldynamics:duct_32:6>]).duration(80).EUt(2).buildAndRegister();
recipes.removeByRecipeName("thermaldynamics:duct_32_13");
recipes.removeByRecipeName("thermaldynamics:duct_32_21");
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_32:5>]).fluidInputs([<liquid:glowstone>*200]).outputs([<thermaldynamics:duct_32:7>]).duration(80).EUt(2).buildAndRegister();
recipes.remove(<thermaldynamics:duct_64>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_64:3>]).fluidInputs([<liquid:aerotheum>*125]).outputs([<thermaldynamics:duct_64>]).duration(50).EUt(2).buildAndRegister();
recipes.remove(<thermaldynamics:duct_64:2>);
fluid_canner.recipeBuilder().inputs([<thermaldynamics:duct_64>]).fluidInputs([<liquid:ender>*1000]).outputs([<thermaldynamics:duct_64:2>]).duration(400).EUt(2).buildAndRegister();


//Fix Compressor recipes for Blocks
recipes.remove(<ore:blockCopper>);
recipes.remove(<ore:blockTin>);
recipes.remove(<ore:blockBronze>);
recipes.remove(<ore:blockApatite>);
recipes.remove(<forestry:apatite>);
compressor.recipeBuilder().inputs([<gregtech:meta_item_1:10071>*9]).outputs([<gregtech:compressed_3:9>]).EUt(2).duration(400).buildAndRegister();
compressor.recipeBuilder().inputs([<gregtech:meta_item_1:10018>*9]).outputs([<gregtech:compressed_0:15>]).EUt(2).duration(400).buildAndRegister();
compressor.recipeBuilder().inputs([<gregtech:meta_item_1:10095>*9]).outputs([<gregtech:compressed_4:10>]).EUt(2).duration(400).buildAndRegister();
compressor.recipeBuilder().inputs([<gregtech:meta_item_1:8226>*9]).outputs([<gregtech:compressed_10:6>]).EUt(2).duration(400).buildAndRegister();

//Hopper replacement
assembler.findRecipe(4,[<minecraft:minecart>,<minecraft:hopper>],[null]).remove();
assembler.recipeBuilder().inputs([<minecraft:minecart>,<pneumaticcraft:omnidirectional_hopper>]).outputs([<minecraft:hopper_minecart>]).EUt(4).duration(400).buildAndRegister();
assembler.findRecipe(2,[<gregtech:meta_item_1:12033>*5,<minecraft:chest>],[null]).remove();
assembler.findRecipe(2,[<gregtech:meta_item_1:12197>*5,<minecraft:chest>],[null]).remove();
assembler.findRecipe(2,[<gregtech:meta_item_1:12033>*5,<minecraft:trapped_chest>],[null]).remove();
assembler.findRecipe(2,[<gregtech:meta_item_1:12197>*5,<minecraft:trapped_chest>],[null]).remove();
assembler.recipeBuilder().inputs([<ore:plateIron>*5,<ore:chestWood>]).outputs([<pneumaticcraft:omnidirectional_hopper>]).EUt(2).duration(800).buildAndRegister();
assembler.recipeBuilder().inputs([<ore:platePigIron>*5,<ore:chestWood>]).outputs([<pneumaticcraft:omnidirectional_hopper>]).EUt(2).duration(800).buildAndRegister();
macerator.findRecipe(8,[<minecraft:hopper>],[null]).remove();
macerator.recipeBuilder().inputs([<pneumaticcraft:omnidirectional_hopper>]).outputs([<gregtech:meta_item_1:2033>*5,<gregtech:meta_item_1:2196>*8]).EUt(8).duration(150).buildAndRegister();
fluid_extractor.findRecipe(32,[<minecraft:hopper>],[null]).remove();
fluid_extractor.recipeBuilder().inputs([<pneumaticcraft:omnidirectional_hopper>]).fluidOutputs([<liquid:iron>*720]).EUt(32).duration(400).buildAndRegister();

//Patch Door Dupes
macerator.findRecipe(8,[<minecraft:wooden_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:spruce_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:birch_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:jungle_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:acacia_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:dark_oak_door>],[null]).remove();
macerator.findRecipe(8,[<minecraft:iron_door>],[null]).remove();
fluid_extractor.findRecipe(32,[<minecraft:iron_door>],[null]).remove();
arc.findRecipe(32,[<minecraft:iron_door>],[<liquid:oxygen>*360]).remove();
plasma_arc.findRecipe(10,[<minecraft:iron_door>],[<liquid:plasma.argon>]).remove();
plasma_arc.findRecipe(10,[<minecraft:iron_door>],[<liquid:plasma.nitrogen>]).remove();

//Flexible Casing
assembler.recipeBuilder().inputs(<ore:plateBronze>*4,<ore:plateEmerald>*2,<forestry:impregnated_casing>).fluidInputs(<liquid:glass>*200).outputs(<forestry:flexible_casing>).EUt(30).duration(20).buildAndRegister();